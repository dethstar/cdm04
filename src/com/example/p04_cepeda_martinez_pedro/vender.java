/*Practica 04 para la materia CDM
 *Alumno: Pedro Vladimir Cepeda Martinez
 * 19/Nov/2013
 * */
package com.example.p04_cepeda_martinez_pedro;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class vender extends Activity {
	
	dataWrapper carrosWraper;
	TextView precio;
	String fabElegido;
	carro carroSelected;
	Button vender; 
	Spinner lv;
	File fichero;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vender);
		fichero = new File(Environment.getExternalStorageDirectory() + "/CDM_P04_CEPEDA_MARTINEZ_PEDRO/","carritos.txt");
		Bundle extras = getIntent().getExtras();
		if (extras != null)
		{
		    carrosWraper = (dataWrapper)extras.getSerializable("carros");
		    ArrayList<String> fabricantes = extras.getStringArrayList("fabricantes");
		    Spinner fab = (Spinner) findViewById(R.id.fabricante);
		    lv = (Spinner) findViewById(R.id.modelos);
		    precio = (TextView) findViewById(R.id.precio);
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, fabricantes);
			fab.setAdapter(arrayAdapter);
			fab.setOnItemSelectedListener(new fabSelector());
			lv.setOnItemSelectedListener(new modelSelector());
		}
		vender = (Button)findViewById(R.id.vender);
		Button ventas = (Button)findViewById(R.id.btnSig);
		ventas.setOnClickListener(new Button.OnClickListener() {
			   public void onClick(View v) {
				   	Intent intent = new Intent(vender.this, ventas.class);
	               	intent.putExtra("carros", carrosWraper);
	               	startActivity(intent);
	               	
			   }
		});
		vender.setOnClickListener(new Button.OnClickListener() {
			   public void onClick(View v) {
				   carroSelected.setVentas(carroSelected.getVentas()+1);
				   precio.setText("Precio: "+carroSelected.getPrecio()+"\nTotal Vendidos: "+carroSelected.getVentas());
					try {
            			String res="";
            			for(carro c: carrosWraper.carros){
            				res+=""+c.getNombre()+" "+
                    				c.getModelo()+" "+
                    				c.getFabricante()+" "+
                    				c.getPrecio()+" "+c.isMexicano()+" "+c.isNuevo()+" "+c.getVentas()+"\n";
            			}
            			writeToFile(res,fichero.getAbsolutePath());
    				} catch (Exception e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
				   Toast.makeText(getApplicationContext(), "Vendido",
						   Toast.LENGTH_LONG).show();
			   }
		});
	}
	private void writeToFile(String data, String f) {
	    try {
	    	BufferedWriter output = new BufferedWriter(new FileWriter(f));
	    	output.write(data);
	        output.close();
	    }
	    catch (IOException e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } 
	}
	public void populateModels(List<String> carritosDeFab){
		  ArrayAdapter<String> arrayAdapter =      
 		         new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, carritosDeFab);
 		         lv.setAdapter(arrayAdapter);
	}
	public class fabSelector implements OnItemSelectedListener {

	    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
	    	fabElegido=parent.getItemAtPosition(pos).toString();
	    	List<String> carritosDeFab = new ArrayList<String>();
	    	int i=0;
	    	if(carrosWraper.carros!=null){
	    	for(carro c : carrosWraper.carros) {
				if(c.getFabricante().equals(fabElegido)){
					carritosDeFab.add(c.getNombre()+" "+c.getModelo());
					i++;
				}
			}
	    	populateModels(carritosDeFab);
	    	if(i>0){
	    		
	    		vender.setEnabled(true);
	    	}
	    	else
	    		vender.setEnabled(false);
	    	}
	    	/*
	    	 * aqui buscar los carros que tengan parent.getItemAtPosition(pos).toString() como fabricante
	    	 * crear arreglo
	    	 * agregar al adaptador del spinner lv (modelos) con el siguiente formato:
	    	 * Nombre | Modelo
	    	 * precio.setText(parent.getItemAtPosition(pos).toString());
	        */
	    }

	    public void onNothingSelected(AdapterView parent) {
	        // Do nothing.
	    }
	}
	public class modelSelector implements OnItemSelectedListener {
		
		 public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			 String[] nombreModelo=parent.getItemAtPosition(pos).toString().split(" ");
			 for(int i =0 ; i<carrosWraper.carros.size();i++) {
					if(carrosWraper.carros.get(i).getFabricante().equals(fabElegido)){
						if(carrosWraper.carros.get(i).getModelo().equals(nombreModelo[1]) && carrosWraper.carros.get(i).getNombre().equals(nombreModelo[0])){
							carroSelected=carrosWraper.carros.get(i);
						}
					}
				}
			 	precio.setText("Precio: "+carroSelected.getPrecio()+"\nTotal Vendidos: "+carroSelected.getVentas());
			 	/*			
		    	 * encuentra el auto seleccionado en la lista
		    	 * con un apuntadorsillo elegir el carrito en caso de que aplanen vender nadamas aumentar en ese carrito ;)
		    	 * */
		    }

		    public void onNothingSelected(AdapterView parent) {
		    	
		    }
	}
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
