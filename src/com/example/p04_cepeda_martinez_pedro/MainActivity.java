package com.example.p04_cepeda_martinez_pedro;
/*Practica 04 para la materia CDM
 *Alumno: Pedro Vladimir Cepeda Martinez
 * 19/Nov/2013
 * */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private Button btnAdd;
	private EditText nombre,precio,modelo;
	public List<String> Fabricantes;
	public List<carro> carros;
	public String selectedFromList; 
	public ListView lv;
	public File fichero;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		carros = new LinkedList<carro>();
		if(crearDirectorio()){
			fichero = new File(Environment.getExternalStorageDirectory() + "/CDM_P04_CEPEDA_MARTINEZ_PEDRO/","carritos.txt");
			if(fichero.exists()){
					try {
						Scanner scnf= new Scanner(fichero);
						while(scnf.hasNext()){
							String[] aux=scnf.nextLine().split(" ");
							carro auxC=new carro(aux[0],aux[1],aux[2],Float.parseFloat(aux[3]),Boolean.parseBoolean(aux[4]),Boolean.parseBoolean(aux[5]));
							auxC.setVentas(Integer.parseInt(aux[6]));
							carros.add(auxC);
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
			
				//leer(carritos);
			}else{
				try {
					fichero.createNewFile();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		Fabricantes = new ArrayList<String>();
		
		Fabricantes.add("Ford");
		Fabricantes.add("Ferrari");
		Fabricantes.add("General Motors");
		lv = (ListView) findViewById(R.id.fab);
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, Fabricantes);
		lv.setAdapter(arrayAdapter); 
		nombre = (EditText)findViewById(R.id.nombre);
    	precio = (EditText)findViewById(R.id.precio);
    	modelo = (EditText)findViewById(R.id.modelo);
		btnAdd = (Button)findViewById(R.id.btnAdd);
		Button ventas = (Button)findViewById(R.id.go2Ventas);
		btnAdd.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
            	RadioGroup g = (RadioGroup) findViewById(R.id.rGroup);
            	int selected = g.getCheckedRadioButtonId();
            	RadioButton b = (RadioButton) findViewById(selected);
            	boolean nuevo= b.getText().toString().equals("Nuevo")? true:false;
            	boolean mexican = ((CheckBox) findViewById(R.id.isMexican)).isChecked();
            	if(validar(nombre.getText().toString(),precio.getText().toString(),modelo.getText().toString(),selectedFromList)){
            		carros.add(new carro(
            				nombre.getText().
            				toString(),modelo.getText().toString(),selectedFromList,
            				Float.parseFloat(precio.getText().toString()),mexican,nuevo
            				));
            		try {
            			String res="";
            			for(carro c: carros){
            				res+=""+c.getNombre()+" "+
                    				c.getModelo()+" "+
                    				c.getFabricante()+" "+
                    				c.getPrecio()+" "+c.isMexicano()+" "+c.isNuevo()+" "+c.getVentas()+"\n";
            			}
            			writeToFile(res,fichero.getAbsolutePath());
    				} catch (Exception e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
            		Toast.makeText(getApplicationContext(), "Vehiculo Agregado",
         				   Toast.LENGTH_LONG).show();
            	}else{
            		Toast.makeText(getApplicationContext(), "Dato Faltante (tap en el nombre del fabricante)",
          				   Toast.LENGTH_LONG).show();
            	}
            	
            }
        });
		ventas.setOnClickListener(new Button.OnClickListener() {
			   public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, vender.class);
               	intent.putExtra("carros", new dataWrapper(carros));
                intent.putStringArrayListExtra("fabricantes", (ArrayList<String>) Fabricantes);
               	int CodigoPeticion;
               	CodigoPeticion=2;
               	startActivityForResult (intent,CodigoPeticion); 
			   }
		});
		
		lv.setOnItemClickListener(new OnItemClickListener() {
		      public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
		    	  selectedFromList=(String) (lv.getItemAtPosition(myItemInt)); 
		      }                 
		});
	}
	private void writeToFile(String data, String f) {
	    try {
	    	BufferedWriter output = new BufferedWriter(new FileWriter(f));
	    	output.write(data);
	        output.close();
	    }
	    catch (IOException e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } 
	}
	private boolean crearDirectorio(){
		File folder = new File(Environment.getExternalStorageDirectory() + "/CDM_P04_CEPEDA_MARTINEZ_PEDRO");
		boolean success=true;
		if (!folder.exists()) {
		    success = folder.mkdir();
		}else{
			return true;
		}
		return success;
	}
	private boolean validar(String un, String dos, String tres, String fab){
		//valida que los campos esten elegidos
		if(un.equals("") || dos.equals("") || tres.equals("") || fab==null)
			return false;
		else
			return true;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
