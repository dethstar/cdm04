/*Practica 04 para la materia CDM
 *Alumno: Pedro Vladimir Cepeda Martinez
 * 19/Nov/2013
 * */
package com.example.p04_cepeda_martinez_pedro;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ventas extends Activity{
	public String[] carros;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ventas);
		TableLayout tl = (TableLayout) findViewById(R.id.tl);
	
		Bundle extras = getIntent().getExtras();
		if (extras != null)
		{
			dataWrapper carrosWraper = (dataWrapper)extras.getSerializable("carros");
			carros = new String[carrosWraper.carros.size()];
			if(carrosWraper.carros.size()>0){
				int i=0;
				for(carro c: carrosWraper.carros){
					//agregar cada carro
					TableRow tr = new TableRow(this);
		            tr.setId(100+i);
		            TextView aux = new TextView(this);
		            aux.setText(c.getNombre());
		            aux.setTextSize(18.0f);
		            TextView aux1 = new TextView(this);
		            aux1.setText(""+c.getPrecio());
		            TextView aux2 = new TextView(this);
		            aux2.setText(""+c.getVentas());
		            aux2.setTextSize(18.0f);
		            tr.addView(aux);
		            tr.addView(aux1);
		            tr.addView(aux2);
		            tl.addView(tr, new TableLayout.LayoutParams(
		                    LayoutParams.FILL_PARENT,
		                    LayoutParams.WRAP_CONTENT));
					i++;
				}
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, carros);
				//gridView.setAdapter(adapter);
			}else{
				Toast.makeText(getApplicationContext(), "No hay carritos",Toast.LENGTH_LONG).show();
			}
			
		}
		else{
			Toast.makeText(getApplicationContext(), "No hay carritos",Toast.LENGTH_LONG).show();
		}
	}
	public void carros2Array(List<carro> c){
		for(int i=0;i<carros.length;i++){
			carros[i]=c.get(i).getNombre()+"\n"+c.get(i).getPrecio()+"\nVendidos: "+c.get(i).getVentas();
		}
	}
	public boolean onCreateOptionsMenu(Menu menu){
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
