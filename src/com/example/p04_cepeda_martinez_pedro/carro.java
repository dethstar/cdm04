/*Practica 03 para la materia CDM
 *Alumno: Pedro Vladimir Cepeda Martinez
 * 7/Nov/2013
 * */
package com.example.p04_cepeda_martinez_pedro;

import java.io.Serializable;

public class carro implements Serializable {
	private String nombre,modelo,fabricante;
	private float precio;
	private boolean mexicano,nuevo;
	private int ventas=0;
	carro(String nombre, String modelo, String fabricante, float precio, boolean mexicano, boolean nuevo){
		this.fabricante=fabricante;
		this.mexicano=mexicano;
		this.nombre=nombre;
		this.modelo=modelo;
		this.precio=precio;
		this.nuevo=nuevo;
	}
	carro(){
		
	}
	public boolean isMexicano() {
		return mexicano;
	}
	public void setMexicano(boolean mexicano) {
		this.mexicano = mexicano;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public boolean isNuevo() {
		return nuevo;
	}
	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}
	public int getVentas() {
		return ventas;
	}
	public void setVentas(int ventas) {
		this.ventas = ventas;
	}
	
	

}
